# Tensor network contraction

My personal research project, devoting to solving a combinatorial optimization task of finding the best tensor contraction path (task description is here https://arxiv.org/pdf/2002.01935.pdf) using Reinforcement Learning (AlphaZero) & Graph Neural Network techniques. JAX (Haiku, Jraph) is used as deep learning framework and WandB as experiments tracker.
