from typing import Tuple, Union
import numpy as np
import dm_env
import time


class MinimalLossAgent:
    def __init__(self, log_mode: bool = False):
        self.log_mode = log_mode

    def select_action(self, timestep: dm_env.TimeStep, select_best: bool = False,
                      return_logits: bool = False) -> Union[int, Tuple[int, np.ndarray]]:
        """Selects action with minimal current loss."""

        graph_tuple = timestep.observation

        senders = np.array(graph_tuple.senders)
        receivers = np.array(graph_tuple.receivers)
        edges = np.array(graph_tuple.edges)

        max_node_index = max(np.amax(senders), np.amax(receivers))

        node_dims = np.ones(max_node_index + 1)
        for i in range(len(edges)):
            u, v, cost = senders[i], receivers[i], edges[i][0]
            if self.log_mode:
                node_dims[u] += cost
                node_dims[v] += cost
            else:
                node_dims[u] *= cost
                node_dims[v] *= cost
        best_action_value = np.inf
        best_action = None
        for i in range(len(edges)):
            u, v, cost = senders[i], receivers[i], edges[i][0]
            contraction_cost = self.contraction_cost(node_dims[u], node_dims[v], cost)
            if best_action_value > contraction_cost:
                best_action_value = contraction_cost
                best_action = i
        assert best_action is not None
        if return_logits:
            logits = np.zeros(len(edges))
            logits[best_action] = 100
            return int(best_action), logits
        return int(best_action)

    def update(self, timestep: dm_env.TimeStep, action: int, new_timestep: dm_env.TimeStep):
        pass

    def contraction_cost(self, u_dims, v_dims, edge_cost):
        if self.log_mode:
            return u_dims + v_dims - edge_cost
        else:
            return u_dims * v_dims / edge_cost


class MaximumEdgeAgent:
    def __init__(self):
        pass

    def select_action(self, timestep: dm_env.TimeStep, select_best: bool = False,
                      return_logits: bool = False) -> Union[int, Tuple[int, np.ndarray]]:
        """Selects action with maximum edge cost."""
        edges = np.array(timestep.observation.edges)
        best_action = 0
        for i in range(len(edges)):
            if edges[i][0] > edges[best_action][0]:
                best_action = i
        if return_logits:
            logits = np.zeros(len(edges))
            logits[best_action] = 100
            return int(best_action), logits
        return int(best_action)

    def update(self, timestep: dm_env.TimeStep, action: int, new_timestep: dm_env.TimeStep):
        pass
