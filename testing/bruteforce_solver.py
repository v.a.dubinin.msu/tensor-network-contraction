from typing import List
import copy

from tensor_networks.tensor_graph import TensorGraph


def solve(graph: TensorGraph, current_result: int, all_possible_results: List[int]):
    if graph.nodes_count() == 1:
        all_possible_results.append(current_result)
        return
    for edge in graph.edges:
        new_graph = copy.deepcopy(graph)
        solve(new_graph, current_result + new_graph.contract(edge), all_possible_results)
