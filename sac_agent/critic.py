from typing import Tuple

import jax
import jax.numpy as jnp

import haiku as hk


from tensor_networks.utils import get_segment_ids, my_partition_softmax


@jax.jit
def target_update(target_critic_params: hk.Params, critic_params: hk.Params, tau: float) -> hk.Params:
    new_target_params = jax.tree_multimap(lambda p, tp: p * tau + tp * (1 - tau), target_critic_params, critic_params)
    return new_target_params


def critic_loss_fn(actor_forward, critic_forward, target_critic_forward, actor_params, critic_params, target_critic_params,
                   batch, temperature, discount, trajectory_edges_total_count) -> Tuple[jnp.ndarray, dict]:
    next_observations = batch.next_observations
    logits = actor_forward(actor_params, next_observations).squeeze()
    n_partitions, segment_ids = get_segment_ids(next_observations.n_edge, trajectory_edges_total_count)
    softmax = my_partition_softmax(logits, next_observations.n_edge,
                                   sum_partitions=trajectory_edges_total_count, return_logsoftmax=False)
    log_softmax = my_partition_softmax(logits, next_observations.n_edge,
                                       sum_partitions=trajectory_edges_total_count, return_logsoftmax=True)
    expected_log_policy = jax.ops.segment_sum(jnp.multiply(log_softmax, softmax), segment_ids, n_partitions,
                                              indices_are_sorted=True)[:-1]

    next_q1, next_q2 = target_critic_forward(target_critic_params, next_observations)
    next_q1 = next_q1.squeeze()
    next_q2 = next_q2.squeeze()
    next_q = jnp.minimum(next_q1, next_q2)
    expected_q_target = jax.ops.segment_sum(jnp.multiply(next_q, softmax), segment_ids, n_partitions, indices_are_sorted=True)[:-1]

    target_q = jnp.array(batch.rewards) + discount * (expected_q_target - temperature * expected_log_policy)

    observations = batch.observations
    q1, q2 = critic_forward(critic_params, observations)
    q1 = q1.squeeze()
    q2 = q2.squeeze()
    action_indices = jnp.array(batch.actions) + jnp.cumsum(jnp.roll(observations.n_edge[:-1], 1).at[0].set(0))
    q1 = q1[action_indices]
    q2 = q2[action_indices]
    critic_loss = ((q1 - target_q) ** 2 + (q2 - target_q) ** 2).mean()
    return critic_loss, {
        'critic_loss': critic_loss,
        'q1': q1.mean(),
        'q2': q2.mean()
    }
