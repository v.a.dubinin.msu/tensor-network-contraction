from typing import NamedTuple, Callable, Any, Sequence, Tuple, Union
import time
import numpy as np

from bsuite.baselines import base

import dm_env
import haiku as hk
import jax
import jax.numpy as jnp
import optax
import jraph


from tensor_networks.gnn import gnn_definition, double_gnn
from tensor_networks.testing.graph_generators import fully_connected_graph
from tensor_networks.utils import graph_to_tuple, my_batch, convert_tuple_to_jnp
from tensor_networks.replay_buffer import ReplayBuffer, Batch

from tensor_networks.sac_agent import actor, critic
from tensor_networks.sac_agent.temperature import TemperatureModel


class ModelState(NamedTuple):
    params: hk.Params
    opt_state: Any


def init_net(net: Callable[..., Any], rng: jax.random.PRNGKey,
             optimizer_lr: float, dummy_input: jnp.array) -> Tuple[ModelState, Callable[..., Any], optax.GradientTransformation]:
    init, forward = hk.without_apply_rng(hk.transform(net))
    params = init(rng, dummy_input)
    optimizer = optax.adam(learning_rate=optimizer_lr)
    opt_state = optimizer.init(params)
    return ModelState(params=params, opt_state=opt_state), forward, optimizer


class SoftActorCritic:
    def __init__(
            self,
            actor_net,
            critic_net,
            actor_lr: float,
            critic_lr: float,
            temperature_lr: float,
            rng: jax.random.PRNGKey,
            discount: float,
            tau: float,
            target_entropy: float,
            max_nodes_count: int,
            max_edges_count: int,
            batch_size: int,
            buffer_capacity: int,
            pretrained_iterations_count: int,
            update_period: int,
            target_critic_update_period: int = 5
    ):
        trajectory_edges_total_count = max_edges_count * batch_size

        self.rng, actor_key, critic_key, target_critic_key = jax.random.split(rng, 4)
        dummy_graph_tuple = graph_to_tuple(fully_connected_graph(2, 1))

        self.actor_state, actor_forward, actor_optimizer = init_net(actor_net, actor_key, actor_lr, dummy_graph_tuple)
        self.critic_state, critic_forward, critic_optimizer = init_net(critic_net, critic_key, critic_lr, dummy_graph_tuple)
        self.target_critic_state, target_critic_forward, _ = init_net(critic_net, target_critic_key, critic_lr, dummy_graph_tuple)

        actor_loss = lambda actor_params, critic_params, batch, temperature: actor.actor_loss_fn(actor_forward, critic_forward, actor_params,
                                                                                                      critic_params, batch, temperature, trajectory_edges_total_count)

        critic_loss = lambda actor_params, critic_params, target_critic_params, batch, temperature: critic.critic_loss_fn(actor_forward, critic_forward, target_critic_forward,
                                                                                                                               actor_params, critic_params, target_critic_params,
                                                                                                                               batch, temperature, discount, trajectory_edges_total_count)

        def sgd_step(loss_fn: Callable[..., Any], all_models_states: Sequence[ModelState], main_state_index: int,
                     optimizer: optax.GradientTransformation, batch: Batch, temperature: float) -> Tuple[ModelState, dict]:
            """Does a step of SGD over a batch."""
            all_params = (model_state.params for model_state in all_models_states)
            main_state = all_models_states[main_state_index]
            gradients, info = jax.grad(loss_fn, argnums=main_state_index, has_aux=True)(*all_params, batch, temperature)
            updates, new_opt_state = optimizer.update(gradients, main_state.opt_state)
            new_params = optax.apply_updates(main_state.params, updates)
            return ModelState(params=new_params, opt_state=new_opt_state), info

        actor_sgd_step = jax.jit(lambda actor_state, critic_state, batch, temperature: \
                                 sgd_step(actor_loss, (actor_state, critic_state), 0, actor_optimizer, batch, temperature))
        critic_sgd_step = jax.jit(lambda actor_state, critic_state, target_critic_state, batch, temperature: \
                                  sgd_step(critic_loss, (actor_state, critic_state, target_critic_state), 1, critic_optimizer, batch, temperature))

        def _update(actor_state: ModelState, critic_state: ModelState, target_critic_state: ModelState,
                    batch: Batch, temperature: float, update_target: bool) -> Tuple[ModelState, ModelState, ModelState, float]:
            new_critic_state, _ = critic_sgd_step(actor_state, critic_state, target_critic_state, batch, temperature)
            if update_target:
                new_target_critic_state = ModelState(params=critic.target_update(target_critic_state.params, new_critic_state.params, tau),
                                                     opt_state=target_critic_state.opt_state)
            else:
                new_target_critic_state = target_critic_state
            new_actor_state, actor_info = actor_sgd_step(actor_state, new_critic_state, batch, temperature)
            return new_actor_state, new_critic_state, new_target_critic_state, actor_info['normalized_entropy']

        self.updates_calls = 0
        self.updates_count = 0
        self.target_critic_update_period = target_critic_update_period
        self.target_entropy = target_entropy
        self._update = _update
        self.forward = jax.jit(actor_forward)
        self.temperature_model = TemperatureModel(temperature_lr)
        self.max_nodes_count = max_nodes_count
        self.max_edges_count = max_edges_count
        self.batch_size = batch_size
        self.pretrained_iterations_count = pretrained_iterations_count
        self.update_period = update_period
        self._buffer = ReplayBuffer(capacity=buffer_capacity)

    def select_action(self, timestep: dm_env.TimeStep, select_best: bool = False,
                      return_logits: bool = False) -> Union[int, Tuple[int, jnp.ndarray]]:
        if self.updates_calls < self.pretrained_iterations_count:
            total_actions_count = np.shape(timestep.observation.senders)[0]
            action = np.random.randint(total_actions_count)
            if return_logits:
                distribution = np.zeros(total_actions_count)
                distribution[action] = 100
                return action, distribution
            return action

        """Selects actions according to a softmax policy."""
        key, self.rng = jax.random.split(self.rng, 2)
        observation = jraph.pad_with_graphs(timestep.observation,
                                            n_node=self.max_nodes_count + 1,
                                            n_edge=self.max_edges_count,
                                            n_graph=2)
        logits = self.forward(self.actor_state.params, observation).squeeze()[:timestep.observation.n_edge[0]]
        logits = logits.reshape(1, -1)
        if select_best:
            action = jax.numpy.argmax(logits).squeeze()
        else:
            action = jax.random.categorical(key, logits).squeeze()
        if return_logits:
            return int(action), logits.reshape(-1)
        return int(action)

    def update(
            self,
            timestep: dm_env.TimeStep,
            action: int,
            new_timestep: dm_env.TimeStep,
    ):
        self._buffer.insert(timestep.observation, action, new_timestep.reward, new_timestep.observation)
        if self.updates_calls >= self.pretrained_iterations_count:
            update_this_iteration = ((self.updates_calls - self.pretrained_iterations_count) % self.update_period == 0)
            if update_this_iteration:
                batch = self._buffer.get_batch(self.batch_size)
                batch = self._prepared_batch(batch)
                cuda_batch = batch._replace(observations=convert_tuple_to_jnp(batch.observations),
                                            next_observations=convert_tuple_to_jnp(batch.next_observations))
                update_target = (self.updates_count % self.target_critic_update_period == 0)
                self.actor_state, self.critic_state, self.target_critic_state, normalized_entropy = self._update(self.actor_state,
                                                                                                                 self.critic_state,
                                                                                                                 self.target_critic_state,
                                                                                                                 cuda_batch,
                                                                                                                 self.temperature_model(),
                                                                                                                 update_target)
                self.temperature_model.update(normalized_entropy, self.target_entropy)
                self.updates_count += 1
        self.updates_calls += 1

    def _prepared_batch(self, batch: Batch) -> Batch:
        return Batch(actions=jnp.array(batch.actions, dtype=jnp.int32),
                     rewards=jnp.array(batch.rewards, dtype=jnp.float32),
                     observations=jraph.pad_with_graphs(my_batch(batch.observations, np_=np),
                                                        n_node=self.max_nodes_count * self.batch_size + 1,
                                                        n_edge=self.max_edges_count * self.batch_size,
                                                        n_graph=self.batch_size + 1),
                     next_observations=jraph.pad_with_graphs(my_batch(batch.next_observations, np_=np),
                                                             n_node=self.max_nodes_count * self.batch_size + 1,
                                                             n_edge=self.max_edges_count * self.batch_size,
                                                             n_graph=self.batch_size + 1))


def default_sac_agent(max_nodes_count: int, max_edges_count: int, actor_lr: float = 1e-3, critic_lr: float = 1e-3,
                      temperature_lr: float = 1e-3, discount: float = 0.99, tau: float = 0.99,
                      target_entropy: float = 0.3, batch_size: int = 100, buffer_capacity: int = 1000,
                      pretrained_iterations_count: int = 500, update_period = 1,
                      target_critic_update_period: int = 5, seed: int = 0) -> base.Agent:
    return SoftActorCritic(
        actor_net=gnn_definition,
        critic_net=double_gnn,
        actor_lr=actor_lr,
        critic_lr=critic_lr,
        temperature_lr=temperature_lr,
        rng=jax.random.PRNGKey(seed),
        discount=discount,
        tau=tau,
        target_entropy=target_entropy,
        max_nodes_count=max_nodes_count,
        max_edges_count=max_edges_count,
        batch_size=batch_size,
        buffer_capacity=buffer_capacity,
        pretrained_iterations_count=pretrained_iterations_count,
        update_period=update_period,
        target_critic_update_period=target_critic_update_period
    )
