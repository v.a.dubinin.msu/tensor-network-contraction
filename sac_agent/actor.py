from typing import Tuple

import jax
import jax.numpy as jnp

from tensor_networks.utils import my_partition_softmax, get_segment_ids


def actor_loss_fn(actor_forward, critic_forward, actor_params, critic_params,
                  batch, temperature, trajectory_edges_total_count) -> Tuple[jnp.ndarray, dict]:
    observations = batch.observations
    batch_size = len(observations.n_node) - 1  # Minus one due to jraph padding - last graph in batch is only needed
                                               # for making nodes and edges count the same for all batches.
    logits = actor_forward(actor_params, observations).squeeze()
    softmax = my_partition_softmax(logits, observations.n_edge,
                                   sum_partitions=trajectory_edges_total_count, return_logsoftmax=False)
    log_softmax = my_partition_softmax(logits, observations.n_edge,
                                       sum_partitions=trajectory_edges_total_count, return_logsoftmax=True)
    q1, q2 = critic_forward(critic_params, observations)
    q1 = q1.squeeze()
    q2 = q2.squeeze()
    q = jnp.minimum(q1, q2)
    n_partitions, segment_ids = get_segment_ids(observations.n_edge, trajectory_edges_total_count)
    batch_entropy = jax.ops.segment_sum(-jnp.multiply(softmax, log_softmax), segment_ids, n_partitions,
                                        indices_are_sorted=True)[:-1]
    total_entropy = jnp.sum(batch_entropy)
    actor_loss = -(jnp.dot(q, softmax) + temperature * total_entropy) / batch_size
    normalized_entropy = jnp.mean(batch_entropy / jnp.log(jnp.clip(observations.n_edge[:-1], a_min=2)))
    return actor_loss, {
        'actor_loss': actor_loss,
        'normalized_entropy': normalized_entropy
    }
