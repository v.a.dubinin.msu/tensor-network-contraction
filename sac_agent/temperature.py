import jax
import optax
import jax.numpy as jnp


class TemperatureModel:
    def __init__(self, optimizer_lr: float):
        self.log_temp = 0.0
        self.optimizer = optax.adam(learning_rate=optimizer_lr)
        self.opt_params = self.optimizer.init(self.log_temp)

    def __call__(self) -> jnp.ndarray:
        return jnp.exp(self.log_temp)

    def _sgd_step(self, loss_fn):
        gradients, _ = jax.grad(loss_fn, has_aux=True)(self.log_temp)
        updates, new_opt_params = self.optimizer.update(gradients, self.opt_params)
        new_log_temp = optax.apply_updates(self.log_temp, updates)
        self.log_temp = new_log_temp
        self.opt_params = new_opt_params

    def update(self, entropy: float, target_entropy: float):
        def temperature_loss_fn(log_temp_params):
            temp_params = jnp.exp(log_temp_params)
            temp_loss = temp_params * (entropy - target_entropy)
            return temp_loss, {'temperature': temp_params, 'temp_loss': temp_loss}

        self._sgd_step(temperature_loss_fn)
