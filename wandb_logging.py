from typing import List, Tuple
import numpy as np
import wandb

from tensor_networks.train import AgentTrajectory
from tensor_networks.utils import Graph, Edge


# Careful! Now function return not the whole trajectory, but only several last observations.
# It's due to strange logging behavior of wandb - large distributions saved as histograms,
# and it's impossible to restore initial distribution vector from the hist.

def trajectory_from_wandb_list(wandb_list: List) -> AgentTrajectory:
    observations = []
    distributions = []
    for graph_list in wandb_list[0]:
        edges = []
        for edge_list in graph_list[1]:
            edges.append(Edge(id=edge_list[0], u=edge_list[1], v=edge_list[2], dim=edge_list[3]))
        graph = Graph(nodes_count=graph_list[0], edges=edges)
        observations.append(graph)
    cutted_trajectories = 0
    for distribution_list in wandb_list[1]:
        if isinstance(distribution_list, dict):
            cutted_trajectories += 1
        elif not isinstance(distribution_list, list):
             distribution_list = [distribution_list]
        distributions.append(np.array(distribution_list))
    chosen_actions = wandb_list[2]
    return AgentTrajectory(observations=observations[cutted_trajectories:],
                           distributions=distributions[cutted_trajectories:],
                           chosen_actions=chosen_actions[cutted_trajectories:])


def get_trajectory_from_run(run_path: str, trajectory_id: int) -> Tuple[AgentTrajectory, float]:
    api = wandb.Api()
    run = api.run(run_path)
    if run.state == "finished":
        history = list(run.scan_history(keys=["agent_trajectory", "logloss"]))
        trajectory = trajectory_from_wandb_list(history[trajectory_id]['agent_trajectory'])
        logloss = history[trajectory_id]['logloss']
        return trajectory, logloss
