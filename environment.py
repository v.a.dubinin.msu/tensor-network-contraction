import dm_env
from tensor_graph import TensorGraph
from utils import Graph, graph_to_tuple
import numpy as np


class TensorGraphEnvironment:
    def __init__(self, graph: Graph, log_mode: bool = False):
        self.initial_graph = graph
        self.current_graph = self.initial_graph
        self.log_mode = log_mode
        self.tensor_graph = TensorGraph(graph, log_mode)

    def reset(self) -> TensorGraph:
        self.tensor_graph = TensorGraph(self.initial_graph, self.log_mode)
        self.current_graph = self.initial_graph
        return graph_to_tuple(self.initial_graph)

    def step(self, action: int) -> (Graph, int, int, None):
        action_edge_id = self.current_graph.edges[action].id
        reward = -self.tensor_graph.contract(action_edge_id)
        self.current_graph = self.tensor_graph.get_current_graph()
        return graph_to_tuple(self.current_graph), reward, self.current_graph.nodes_count == 1, None
    # What about env.observation_space.shape, env.observation_space.dtype and env.action_space.n?

    def get_actions_count(self):
        return len(self.current_graph.edges)


class GymEnvWrapper:
    def __init__(self, env, discount=0.99):
        self.env = env
        self.discount = discount

    def reset(self):
        init_state = self.env.reset()
        timestep = dm_env.TimeStep(observation=init_state,
                                   step_type=dm_env.StepType.FIRST,
                                   reward=None,
                                   discount=None)
        return timestep

    def step(self, action):
        new_state, reward, done, info = self.env.step(action)
        step_type = dm_env.StepType.MID
        if done:
            step_type = dm_env.StepType.LAST
        timestep = dm_env.TimeStep(observation=new_state,
                                   step_type=step_type,
                                   reward=reward,
                                   discount=self.discount)
        return timestep

    def sample_action(self):
        actions_count = self.env.get_actions_count()   # Should change to self.env.action_space.n in case of gym
        if actions_count == 0:
            return None
        return np.random.randint(actions_count)

    def observation_spec(self):
        return dm_env.specs.Array(shape=self.env.observation_space.shape,
                                  dtype=self.env.observation_space.dtype)

    def action_spec(self):
        return dm_env.specs.DiscreteArray(self.env.action_space.n)
