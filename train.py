import numpy as np
from typing import Tuple, Union, List
from collections import namedtuple
import time

from bsuite.baselines import experiment

from tensor_networks.environment import GymEnvWrapper
from tensor_networks.agent import ReinforceAgent
from tensor_networks.utils import tuple_to_graph, my_segment_logsoftmax


AgentTrajectory = namedtuple('AgentTrajectory', ['observations', 'distributions', 'chosen_actions'])


def get_current_quality(env: GymEnvWrapper, agent: ReinforceAgent, episodes_count: int = 100,
                        log_mode: bool = False, log_trajectories=False) -> Union[List, Tuple[List, List[AgentTrajectory]]]:
    agent_time = 0
    env_time = 0
    all_attempts = []
    all_agent_trajectories = []
    for _ in range(episodes_count):
        # Run an episode.
        timestep = env.reset()
        trajectory_rewards = []
        result = 0
        agent_trajectory = AgentTrajectory(observations=[], distributions=[], chosen_actions=[])
        agent_trajectory.observations.append(tuple_to_graph(timestep.observation))
        while not timestep.last():
            # Generate an action from the agent's policy.
            select_time = time.time()
            action, logits = agent.select_action(timestep, select_best=True, return_logits=True)
            distribution = my_segment_logsoftmax(logits, np.zeros(logits.shape, dtype=int), return_logsoftmax=False)
            agent_time += time.time() - select_time

            # Step the environment.
            step_time = time.time()
            new_timestep = env.step(action)
            env_time += time.time() - step_time

            # Save agent trajectory
            agent_trajectory.observations.append(tuple_to_graph(new_timestep.observation))
            agent_trajectory.distributions.append(np.array(distribution))
            agent_trajectory.chosen_actions.append(action)

            if not log_mode:
                result += new_timestep.reward
            else:
                trajectory_rewards.append(-new_timestep.reward)
            # Book-keeping.
            timestep = new_timestep
        if log_mode:
            max_reward = np.amax(trajectory_rewards)
            result -= max_reward + np.log(np.sum(np.exp(np.array(trajectory_rewards) - max_reward)))
        all_attempts.append(result)
        all_agent_trajectories.append(agent_trajectory)
    # print("Agent time:", agent_time)
    # print("Env time:", env_time)
    if log_trajectories:
        return all_attempts, all_agent_trajectories
    return all_attempts


def train_agent(env: GymEnvWrapper, agent: ReinforceAgent, epochs_count: int = 10,
                episodes_in_one_epoch: int = 10, log_mode: bool = False, log_quality: bool = False):
    for epoch_idx in range(epochs_count):
        training_start = time.time()
        experiment.run(agent=agent, environment=env, num_episodes=episodes_in_one_epoch)
        print("Training time:", time.time() - training_start)
        get_quality_start = time.time()
        all_attempts = get_current_quality(env, agent, episodes_in_one_epoch, log_mode)
        all_attempts.sort(reverse=True)
        median_result, best_result, worst_result = all_attempts[episodes_in_one_epoch // 2], all_attempts[0], all_attempts[-1]
        if log_quality:
            median_result = -np.log(-median_result)
            best_result = -np.log(-best_result)
            worst_result = -np.log(-worst_result)
        print("Epoch {0}: Median: {1}, Best: {2}, Worst: {3}".format(epoch_idx + 1, median_result, best_result, worst_result))
        print("Get quality time:", time.time() - get_quality_start)
