import numpy as np
import networkx as nx
import pylab
from typing import Tuple

from tensor_networks.utils import Graph, Edge, normalized_graph


def get_next_state(graph: Graph, action: int) -> Tuple[Graph, float]:
    tensor_graph = TensorGraph(graph, log_mode=True, already_logdim=True)
    action_id = graph.edges[action].id
    loss = tensor_graph.contract(action_id)
    return tensor_graph.get_current_graph(), loss


class TensorGraph:
    def __init__(self, graph: Graph, log_mode: bool = False, already_logdim: bool = False):
        self.adj_dict = {}
        # assert (all([0 <= edge.id < len(graph.edges) for edge in graph.edges])) - Do we need this assert?
        self.edges = {}
        for edge in graph.edges:
            if log_mode and not already_logdim:
                self.edges[edge.id] = edge._replace(dim=np.log(edge.dim))
            else:
                self.edges[edge.id] = edge
            if not (edge.u in self.adj_dict):
                self.adj_dict[edge.u] = set()
            if not (edge.v in self.adj_dict):
                self.adj_dict[edge.v] = set()
            self.adj_dict[edge.u].add(edge.id)
            self.adj_dict[edge.v].add(edge.id)
        self.log_mode = log_mode

    def nodes_count(self) -> int:
        return len(self.adj_dict)

    def node_dim(self, node_id: int) -> int:
        if self.log_mode:
            return np.sum([self.edges[edge_id].dim for edge_id in self.adj_dict[node_id]])
        return np.prod([self.edges[edge_id].dim for edge_id in self.adj_dict[node_id]])

    def contract(self, edge_id: int) -> int:
        assert edge_id in self.edges, 'Edge {0} is not a valid action'.format(edge_id)
        edge = self.edges[edge_id]
        u, v, dim = edge.u, edge.v, edge.dim
        contraction_dim = self.contraction_dim(edge)
        self._remove_edge(edge_id)
        # It's important to make sure that u < v because only u stays alive, and in this case the vertices numeration
        # is independent of the contraction path - every vertex number corresponds to the minimal number
        # in the component of the initial graph.
        if u > v:
            u, v = v, u
        adj_v = self.adj_dict[v].copy()
        for v_edge_id in adj_v:
            v_edge = self.edges[v_edge_id]
            new_edge = Edge(v_edge_id, u, self._other_node(v_edge_id, v), v_edge.dim)
            self._remove_edge(v_edge_id)
            self._add_edge(new_edge)
        self.adj_dict.pop(v)
        sorted_adj_u = sorted(list(self.adj_dict[u]), key=lambda edge_id: (self._other_node(edge_id, u), edge_id))
        for index in range(len(sorted_adj_u) - 1):
            current_edge_id = sorted_adj_u[index]
            next_edge_id = sorted_adj_u[index + 1]
            if not (current_edge_id in self.edges and next_edge_id in self.edges):
                continue
            current_v = self._other_node(current_edge_id, u)
            next_v = self._other_node(next_edge_id, u)
            if current_v == next_v:
                if self.log_mode:
                    new_dim = self.edges[current_edge_id].dim + self.edges[next_edge_id].dim
                else:
                    new_dim = self.edges[current_edge_id].dim * self.edges[next_edge_id].dim
                new_edge = Edge(current_edge_id, u, current_v, new_dim)
                self._remove_edge(current_edge_id)
                self._remove_edge(next_edge_id)
                self._add_edge(new_edge)
        return contraction_dim

    def contraction_dim(self, edge: Edge):
        if self.log_mode:
            return self.node_dim(edge.u) + self.node_dim(edge.v) - edge.dim
        else:
            return self.node_dim(edge.u) * self.node_dim(edge.v) / edge.dim

    def get_current_graph(self) -> Graph:
        """
        This function is very slow now!
        But to speed up we need to change
        the whole class structure, so
        let's do it later.

        """
        return normalized_graph(Graph(nodes_count=self.nodes_count(), edges=list(self.edges.values())))

    def draw(self):
        nx_graph = nx.Graph()
        for edge in self.edges.values():
            nx_graph.add_edges_from([(edge.u, edge.v)], dim=edge.dim, id=edge.id)
        edge_labels = nx.get_edge_attributes(nx_graph, 'dim')
        pos = nx.spring_layout(nx_graph)
        nx.draw_networkx_edge_labels(nx_graph, pos, edge_labels=edge_labels)
        nx.draw(nx_graph, pos, node_color='y', with_labels=True)
        pylab.show()

    def _add_edge(self, edge: Edge):
        self.adj_dict[edge.u].add(edge.id)
        self.adj_dict[edge.v].add(edge.id)
        assert (edge.id not in self.edges)
        self.edges[edge.id] = edge

    def _remove_edge(self, edge_id: int):
        edge = self.edges[edge_id]
        self.adj_dict[edge.u].remove(edge_id)
        self.adj_dict[edge.v].remove(edge_id)
        assert (edge_id in self.edges)
        self.edges.pop(edge_id)

    def _other_node(self, edge_id: int, node_id: int) -> int:
        return self.edges[edge_id].u + self.edges[edge_id].v - node_id
