import numpy as np

# Imagine that we want to keep the sum of large losses l_1 + ... + l_n
# and be able to add new addend to this sum. To do so, we keep pair of elements:
# (logmax = log(max(l_1, ..., l_n)), l_1 / logmax + ... + l_n / logmax).


# This function is needed for merging two pairs of this kind.
# We can add a single edge as a pair (ln(edge), 1).

def merge_loss_pairs(first_pair, second_pair):
    if first_pair[0] < second_pair[0]:
        first_pair, second_pair = second_pair, first_pair
    x, y = first_pair
    a, b = second_pair
    return x, y + b / np.exp(x - a)


def add_logedge_to_loss_pair(loss_pair, log_edge):
    return merge_loss_pairs((log_edge, 1), loss_pair)


def get_logloss(loss_pair):
    return loss_pair[0] + np.log(loss_pair[1])
