from collections import namedtuple
from typing import Optional, Tuple

import numpy as np
import jraph
import jax
import jax.numpy as jnp
import jax.tree_util as tree
from bsuite.baselines.utils.sequence import Trajectory
import dm_env

from jraph._src.utils import segment_max, segment_sum

import enum

Graph = namedtuple('Graph', ['nodes_count', 'edges'])
Edge = namedtuple('Edge', ['id', 'u', 'v', 'dim'])


class GraphType(enum.Enum):
    fully_connected = 0
    tree = 1
    random_connected = 2
    unknown = 3


def normalized_graph(graph: Graph) -> Graph:
    edges = []
    for edge in graph.edges:
        u, v = edge.u, edge.v
        if u > v:
            u, v = v, u
        edges.append(Edge(id=edge.id, u=u, v=v, dim=edge.dim))
    edges.sort(key=lambda edge: (edge.u, edge.v, edge.dim))
    return Graph(nodes_count=graph.nodes_count, edges=edges)


def graph_str_representation(graph: Graph) -> str:
    graph = normalized_graph(graph)
    edges_without_id = [(edge.u, edge.v, edge.dim) for edge in graph.edges]
    return str((graph.nodes_count, edges_without_id))


def my_batch(graphs, np_=np):
    """Returns batched graph given a list of graphs and a numpy-like module."""
    # Calculates offsets for sender and receiver arrays, caused by concatenating
    # the nodes arrays.
    offsets = np_.cumsum(np_.array([0] + [np_.sum(g.n_node) for g in graphs[:-1]]))

    def _map_concat(nests):
        concat = lambda *args: np_.concatenate(args)
        return tree.tree_multimap(concat, *nests)

    return jraph.GraphsTuple(
        n_node=np_.concatenate([g.n_node for g in graphs]),
        n_edge=np_.concatenate([g.n_edge for g in graphs]),
        nodes=_map_concat([g.nodes for g in graphs]),
        edges=_map_concat([g.edges for g in graphs]),
        globals=_map_concat([g.globals for g in graphs]),
        senders=np_.concatenate([g.senders + o for g, o in zip(graphs, offsets)]),
        receivers=np_.concatenate(
          [g.receivers + o for g, o in zip(graphs, offsets)]))


def graph_to_tuple(graph: Graph, np_=np) -> jraph.GraphsTuple:
    jax_graph = jraph.GraphsTuple(
        n_node=np_.array([graph.nodes_count]),
        n_edge=np_.array([len(graph.edges)]),
        nodes=np_.zeros((graph.nodes_count, 1), dtype=np_.float32),  # Maybe some non-zero initialization?
        edges=np_.asarray([[graph.edges[i].dim] for i in range(len(graph.edges))], dtype=np_.float32).reshape(-1, 1),
        globals=np_.asarray([[1.0]], dtype=np_.float32),
        senders=np_.asarray([graph.edges[i].u for i in range(len(graph.edges))], dtype=np_.int32),
        receivers=np_.asarray([graph.edges[i].v for i in range(len(graph.edges))], dtype=np_.int32))
    return jax_graph


# Careful! We get graph with new ids!
# This is because GraphTuple doesn't have edge.id, so old edge_ids have lost.
def tuple_to_graph(graph_tuple: jraph.GraphsTuple) -> Graph:
    edges = []
    for edge_id in range(graph_tuple.n_edge[0]):
        edges.append(Edge(id=edge_id, u=graph_tuple.senders[edge_id],
                          v=graph_tuple.receivers[edge_id], dim=graph_tuple.edges[edge_id][0]))
    graph = Graph(nodes_count=graph_tuple.n_node[0], edges=edges)
    return graph


def convert_tuple_to_jnp(tuple):
    tuple = tuple._replace(n_node=jnp.array(tuple.n_node),
                            n_edge=jnp.array(tuple.n_edge),
                            nodes=jnp.array(tuple.nodes, dtype=jnp.float32),
                            edges=jnp.array(tuple.edges, dtype=jnp.float32),
                            globals=jnp.array(tuple.globals, dtype=jnp.float32),
                            senders=jnp.array(tuple.senders, dtype=jnp.int32),
                            receivers=jnp.array(tuple.receivers, dtype=jnp.int32))
    return tuple


class DummyBuffer:
    def __init__(self):
        self._observations = []
        self._actions = []
        self._rewards = []
        self._discounts = []

    def append(
        self,
        timestep: dm_env.TimeStep,
        action: int,
        new_timestep: dm_env.TimeStep,
    ):
        if len(self._observations) == 0:
            self._observations.append(timestep.observation)
        self._observations.append(new_timestep.observation)
        self._actions.append(action)
        self._rewards.append(new_timestep.reward)
        self._discounts.append(new_timestep.discount)

    def drain(self) -> Trajectory:
        trajectory = Trajectory(
            self._observations,
            self._actions,
            self._rewards,
            self._discounts,
        )
        self._observations = []
        self._actions = []
        self._rewards = []
        self._discounts = []
        return trajectory


def get_segment_ids(partitions: jnp.ndarray, total_length: int) -> Tuple[int, jnp.ndarray]:
    n_partitions = len(partitions)
    segment_ids = jnp.repeat(jnp.arange(n_partitions), partitions, axis=0,
                             total_repeat_length=total_length)
    return n_partitions, segment_ids


def my_partition_softmax(logits: jnp.ndarray, partitions: jnp.ndarray,
                         sum_partitions: Optional[int] = None, return_logsoftmax: bool = True) -> jnp.ndarray:
    n_partitions, segment_ids = get_segment_ids(partitions, sum_partitions)
    return my_segment_logsoftmax(logits, segment_ids, n_partitions,
                                 indices_are_sorted=True, return_logsoftmax=return_logsoftmax)


def my_segment_logsoftmax(logits: jnp.ndarray, segment_ids: jnp.ndarray,
                          num_segments: Optional[int] = None,
                          indices_are_sorted: bool = False,
                          unique_indices: bool = False,
                          return_logsoftmax: bool = True) -> jnp.ndarray:
    maxs = segment_max(logits, segment_ids, num_segments, indices_are_sorted,
                       unique_indices)
    logits = logits - maxs[segment_ids]
    # Then take the exp
    exp_logits = jnp.exp(logits)
    # Then calculate the normalizers
    normalizers = jax.ops.segment_sum(exp_logits, segment_ids, num_segments,
                                      indices_are_sorted, unique_indices)
    if not return_logsoftmax:
        return exp_logits / normalizers[segment_ids]

    logsoftmax = logits - jnp.log(normalizers[segment_ids])
    return logsoftmax
