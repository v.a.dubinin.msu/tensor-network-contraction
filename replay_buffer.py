import numpy as np
from collections import namedtuple

import jraph


Batch = namedtuple('Batch', ['observations', 'actions', 'rewards', 'next_observations'])


class ReplayBuffer:
    def __init__(self, capacity: int, max_nodes_count: int = 0, max_edges_count: int = 0):
        self.is_filled = False
        self.index_to_add = 0
        self.capacity = capacity
        self.observations = [[] for _ in range(capacity)]
        self.actions = [-1 for _ in range(capacity)]
        self.rewards = [0.0 for _ in range(capacity)]
        self.next_observations = [[] for _ in range(capacity)]
        self.max_nodes_count = max_nodes_count
        self.max_edges_count = max_edges_count

    def insert(self, observation: jraph.GraphsTuple, action: int,
               reward: float, next_observation: jraph.GraphsTuple):
        self.observations[self.index_to_add] = observation
        self.actions[self.index_to_add] = action
        self.rewards[self.index_to_add] = reward
        self.next_observations[self.index_to_add] = next_observation
        self.index_to_add += 1
        if self.index_to_add == self.capacity:
            self.is_filled = True
            self.index_to_add = 0

    def get_batch(self, batch_size: int) -> Batch:
        elements_to_sample = self.capacity
        if not self.is_filled:
            elements_to_sample = self.index_to_add
        batch_indices = np.random.choice(elements_to_sample, batch_size)
        batch = Batch(observations=[self.observations[index] for index in batch_indices],
                      actions=np.array(self.actions)[batch_indices],
                      rewards=np.array(self.rewards)[batch_indices],
                      next_observations=[self.next_observations[index] for index in batch_indices])
        return batch
