from typing import NamedTuple


class TrainingArgs(NamedTuple):
    mcts_iterations_number: int = 150
    mcts_cpuct: float = 4
    coach_epochs_count: int = 100
    coach_episodes_count: int = 32
    coach_epochs_to_buffer: int = 10
    batch_size: int = 100
    training_iterations_count: int = 300
    current_nets_count: int = 8
    candidate_nets_count: int = 4
    training_on_test_graph: bool = False
    MCTS_test_inference: bool = False
