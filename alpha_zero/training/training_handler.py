import copy
from random import shuffle
import time
import numpy as np
from tqdm import tqdm, trange
from typing import Callable

import wandb
from joblib import Parallel, delayed

from tensor_networks.utils import Graph

from tensor_networks.alpha_zero.net.alpha_zero_net import AlphaZeroNet, save_net
from tensor_networks.alpha_zero.training.args import TrainingArgs
from tensor_networks.logloss import add_logedge_to_loss_pair, get_logloss
from tensor_networks.tensor_graph import get_next_state
from tensor_networks.alpha_zero.training.trajectory_generator import generate_trajectory_samples
from tensor_networks.alpha_zero.training.apg import AnnealingProbabilitiesGenerator


class TrainingHandler:
    def __init__(self, graph_generator: Callable[[], Graph], net: AlphaZeroNet,
                 args: TrainingArgs, project_name: str, config: dict):
        self.graph_generator = graph_generator
        self.net = net
        self.args = args
        self.project_name = project_name
        self.config = config
        self.training_samples_buffer = []

    def learn(self):
        wandb.init(project=self.project_name, config=self.config)

        try:
            probabilities_generator = AnnealingProbabilitiesGenerator(self.args.coach_epochs_count - 1)
            for epoch_idx in trange(self.args.coach_epochs_count):
                start_time = time.time()

                def func_to_parallelize(net, args, graph_generator):
                    return generate_trajectory_samples(net, args, graph_generator)

                training_samples_with_losses = Parallel(n_jobs=-1)(
                    delayed(func_to_parallelize)(self.net, self.args, self.graph_generator)
                    for _ in range(self.args.coach_episodes_count))

                training_samples = []
                losses = []
                for one_run_samples, loss in training_samples_with_losses:
                    training_samples.extend(one_run_samples)
                    losses.append(loss)
                losses = sorted(losses)

                # print("Sampling generation time:", time.time() - start_time)

                self.training_samples_buffer.append(training_samples)
                if len(self.training_samples_buffer) > self.args.coach_epochs_to_buffer:
                    self.training_samples_buffer.pop(0)

                all_training_samples_list = []
                for training_samples_epoch in self.training_samples_buffer:
                    all_training_samples_list.extend(training_samples_epoch)

                start_time = time.time()

                candidate_net = copy.deepcopy(self.net)
                candidate_net.init()
                self.net.init()

                for _ in tqdm(range(self.args.training_iterations_count)):
                    shuffle(all_training_samples_list)
                    candidate_net.train(all_training_samples_list[: self.args.batch_size])
                # print("Training time:", time.time() - start_time)

                start_time = time.time()

                candidate_loss = self.get_mean_loss(candidate_net)
                current_loss = self.get_mean_loss(self.net)

                print("Best loss:", min(current_loss, candidate_loss))

                probability = probabilities_generator.add_value(candidate_loss - current_loss)

                if current_loss > candidate_loss:
                    self.net = candidate_net
                else:
                    if probability > np.random.rand():
                        self.net = candidate_net
                # print("Quality time:", time.time() - start_time)

                losses_to_log = {'current loss': current_loss,
                                 'candidate loss': candidate_loss,
                                 'best loss': min(current_loss, candidate_loss),
                                 'MCTS with exploration loss median': losses[len(losses) // 2]
                                 }
                probabilities_generator_parameters_to_log = probabilities_generator.get_parameters_to_log()
                wandb.log({**losses_to_log, **probabilities_generator_parameters_to_log})

                self.net.deinit()
        finally:
            save_net(self.net, wandb.run.dir)
            wandb.finish()

    def get_mean_loss(self, net: AlphaZeroNet, iterations_to_estimate: int = 200) -> float:
        mean_loss = 0
        for _ in tqdm(range(iterations_to_estimate)):
            current_state = self.graph_generator()
            trajectory_loss = (0, 0)
            while len(current_state.edges) > 0:
                p, _ = net.predict(current_state)
                action = np.argmax(p)
                new_state, current_loss = get_next_state(current_state, action)
                trajectory_loss = add_logedge_to_loss_pair(trajectory_loss, current_loss)
                current_state = new_state
            mean_loss += get_logloss(trajectory_loss)
        return mean_loss / iterations_to_estimate
