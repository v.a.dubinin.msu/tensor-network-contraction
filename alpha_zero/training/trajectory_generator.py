import numpy as np
from typing import Callable, List, Tuple

from tensor_networks.utils import Graph, normalized_graph

from tensor_networks.alpha_zero.net.alpha_zero_net import AlphaZeroNet
from tensor_networks.alpha_zero.training.args import TrainingArgs
from tensor_networks.logloss import add_logedge_to_loss_pair, get_logloss
from tensor_networks.tensor_graph import get_next_state
from tensor_networks.alpha_zero.mcts.mcts import MCTS


def generate_trajectory_samples(net: AlphaZeroNet, args: TrainingArgs, graph_generator: Callable[[], Graph]) \
                           -> Tuple[List[Tuple[Graph, np.ndarray, int, float]], float]:
    net.init()
    mcts = MCTS(net, args, graph_generator.nodes_count)

    train_samples = []
    current_state = graph_generator()
    previous_action = -1
    while len(current_state.edges) > 0:
        mcts_policy = mcts.evaluate_mcts_distribution(current_state, previous_action, temp=1.0)
        action = np.random.choice(len(mcts_policy), p=mcts_policy)
        new_state, current_loss = get_next_state(current_state, action)
        reward_distr_params = mcts.reward_distr_params[mcts.current_root]
        train_samples.append([current_state, mcts_policy, action, reward_distr_params, current_loss])
        current_state = new_state
        previous_action = action

    total_loss = (0, 0)
    resulted_samples = []
    for sample in train_samples[::-1]:
        total_loss = add_logedge_to_loss_pair(total_loss, sample[-1])
        reward_mu, reward_sigma = sample[-2]
        if reward_sigma > 1e-8:
            sample[-1] = (-get_logloss(total_loss) - reward_mu) / reward_sigma
        else:
            sample[-1] = 0.0
        resulted_samples.append((sample[0], sample[1], sample[2], sample[-1]))

    return resulted_samples[::-1], get_logloss(total_loss)
