import numpy as np


class AnnealingProbabilitiesGenerator:
    def __init__(self, total_points_count: int, multiplier: float = 0.9):
        self.total_points_count = total_points_count
        self.multiplier = multiplier
        self.weighted_sum = 0
        self.multipliers_sum = 0
        self.mean = 0
        self.sigma = 0
        self.all_values = []

    def add_value(self, new_value: float) -> float:
        self.weighted_sum = self.weighted_sum * self.multiplier + new_value
        self.multipliers_sum = self.multipliers_sum * self.multiplier + 1
        self.mean = self.weighted_sum / self.multipliers_sum
        self.all_values.append(new_value)
        self._calculate_sigma()
        return self.get_probability(new_value)

    def _calculate_sigma(self):
        variance_sum = 0
        current_coefficient = 1
        for element in self.all_values[::-1]:
            variance_sum += current_coefficient * (element - self.mean) ** 2
            current_coefficient *= self.multiplier
        self.sigma = np.sqrt(variance_sum / self.multipliers_sum)

    def get_probability(self, value: float) -> float:
        if self.sigma < 1e-9:
            return 0.0
        normalized_value = (value - self.mean) / self.sigma
        initial_probability = np.exp(-normalized_value) / (1 + np.exp(-normalized_value))
        probability = initial_probability * (1. - len(self.all_values) / self.total_points_count)
        return probability

    def get_parameters_to_log(self) -> dict:
        value = self.all_values[-1]
        if self.sigma > 1e-9:
            normalized_value = (value - self.mean) / self.sigma
            initial_probability = np.exp(-normalized_value) / (1 + np.exp(-normalized_value))
            probability = initial_probability * (1. - len(self.all_values) / self.total_points_count)
        else:
            normalized_value = 0.0
            initial_probability = 0.0
            probability = 0.0
        return {"current difference:": value,
                "mean": self.mean,
                "sigma": self.sigma,
                "normalized difference": normalized_value,
                "initial_probability": initial_probability,
                "resulted_probability": probability
                }
