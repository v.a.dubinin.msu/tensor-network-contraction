import numpy as np
from typing import Tuple
from tensor_networks.utils import Graph
from tensor_networks.logloss import add_logedge_to_loss_pair, get_logloss


def contract(adj_matrix: np.ndarray, edge: Tuple[int, int]) -> int:
    u, v = edge
    contract_dim = np.sum(adj_matrix[u]) + np.sum(adj_matrix[v]) - adj_matrix[u, v]
    adj_matrix[u,:] = np.add(adj_matrix[u,:], adj_matrix[v,:])
    adj_matrix[:,u] = np.add(adj_matrix[:,u], adj_matrix[:,v])
    adj_matrix[v,:] = 0
    adj_matrix[:,v] = 0
    adj_matrix[u,u] = 0
    return contract_dim


def graph_to_adj_matrix(graph: Graph, max_nodes_count: int) -> np.ndarray:
    adj_matrix = np.zeros((max_nodes_count, max_nodes_count), dtype = np.float32)
    for edge in graph.edges:
        adj_matrix[edge.u, edge.v] = adj_matrix[edge.v, edge.u] = edge.dim
    return adj_matrix


def adj_matrix_logloss_estimation(graph_to_estimate: Graph, max_nodes_count: int, iterations_to_estimate: int = 20) \
                                  -> Tuple[float, float]:
    all_losses = np.zeros(iterations_to_estimate, dtype=np.float32)
    adj_matrix = graph_to_adj_matrix(graph_to_estimate, max_nodes_count)
    all_matrices = np.tile(adj_matrix, [iterations_to_estimate, 1, 1])
    for iteration_idx in np.arange(iterations_to_estimate):
        trajectory_loss = (0, 0)
        current_matrix = all_matrices[iteration_idx]
        for _ in np.arange(graph_to_estimate.nodes_count - 1):
            us, vs = np.nonzero(current_matrix)
            idx = np.random.randint(len(us))
            random_edge = (us[idx], vs[idx])
            trajectory_loss = add_logedge_to_loss_pair(trajectory_loss, contract(current_matrix, random_edge))
        all_losses[iteration_idx] = get_logloss(trajectory_loss)
    return np.mean(all_losses), np.std(all_losses)
