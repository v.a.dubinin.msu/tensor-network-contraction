import math
import time
import numpy as np
from typing import Tuple

from tensor_networks.tensor_graph import TensorGraph
from tensor_networks.utils import Graph

from tensor_networks.alpha_zero.net.alpha_zero_net import AlphaZeroNet
from tensor_networks.alpha_zero.training.args import TrainingArgs
from tensor_networks.logloss import add_logedge_to_loss_pair, get_logloss
from tensor_networks.alpha_zero.mcts.logloss_distribution_estimation import adj_matrix_logloss_estimation


class MCTS:
    def __init__(self, nnet: AlphaZeroNet, args: TrainingArgs, max_nodes_count: int):
        self.nnet = nnet
        self.args = args
        self.max_nodes_count = max_nodes_count

        self.vertices_count = 1
        self.current_root = 0
        self.action_edges = []
        self.action_ids = []

        self.Qsa = []
        self.Nsa = []
        self.Ns = []

        self.Ps = []
        self.reward_distr_params = []

        self.inference_time = 0
        self.reward_distr_params_time = 0
        self.graph_time = 0

    def _init_vertex(self, vertex: int, graph: Graph):
        assert len(self.action_edges) == vertex
        actions_count = len(graph.edges)
        self.action_edges.append(np.zeros(actions_count, dtype=int) - 1)
        self.action_ids.append(np.zeros(actions_count, dtype=int))
        for idx, edge in enumerate(graph.edges):
            self.action_ids[-1][idx] = edge.id
        self.Qsa.append(np.zeros(actions_count, dtype=np.float32))
        self.Nsa.append(np.zeros(actions_count, dtype=int))
        self.Ns.append(0)

    def evaluate_mcts_distribution(self, graph: Graph, previous_action: int, temp: float = 1) -> np.ndarray:
        if previous_action >= 0:
            if self.action_edges[self.current_root][previous_action] < 0:
                self.action_edges[self.current_root][previous_action] = self.vertices_count
                self.vertices_count += 1
            self.current_root = self.action_edges[self.current_root][previous_action]

        self.inference_time = 0
        self.reward_distr_params_time = 0
        self.graph_time = 0
        total_time = time.time()
        for _ in range(self.args.mcts_iterations_number):
            tensor_graph = TensorGraph(graph, log_mode=True, already_logdim=True)
            self._search(self.current_root, tensor_graph)

#       print("Graph getting time:", self.graph_time)
#       print("Inference time:", self.inference_time)
#       print("Distr params time:", self.reward_distr_params_time)
#       print("Total time:", time.time() - total_time)

        counts = [self.Nsa[self.current_root][action] for action in np.arange(len(graph.edges))]

        if temp < 1e-9:
            best_actions = np.array(np.argwhere(counts == np.max(counts))).flatten()
            best_action = np.random.choice(best_actions)
            policy = [0] * len(counts)
            policy[best_action] = 1
            return policy

        counts = [x ** (1. / temp) for x in counts]
        counts_sum = float(sum(counts))
        policy = [x / counts_sum for x in counts]
        return np.array(policy)

    def _search(self, vertex: int, tensor_graph: TensorGraph) -> Tuple[float, float]:
        if len(self.action_edges) == vertex:
            current_time = time.time()
            graph = tensor_graph.get_current_graph()
            self.graph_time += time.time() - current_time

            self._init_vertex(vertex, graph)
            if len(graph.edges) == 0:
                self.Ps.append(np.zeros(0))
                self.reward_distr_params.append((0., 0.))
                return 0., 0.

            current_time = time.time()
            p, q_a = self.nnet.predict(graph)
            self.Ps.append(p)
            self.inference_time += time.time() - current_time
            current_time = time.time()
            mu, sigma = adj_matrix_logloss_estimation(graph, self.max_nodes_count)
            self.reward_distr_params.append((-mu, sigma))
            self.reward_distr_params_time += time.time() - current_time
            return np.min(mu - sigma * q_a), 1

        if len(tensor_graph.edges) == 0:
            return 0., 0.

        states_attractiveness = self.Qsa[vertex] + self.args.mcts_cpuct * self.Ps[vertex] * math.sqrt(
            self.Ns[vertex]) / (1 + self.Nsa[vertex])
        best_action = np.argmax(states_attractiveness)

        if self.action_edges[vertex][best_action] < 0:
            self.action_edges[vertex][best_action] = self.vertices_count
            self.vertices_count += 1

        next_vertex = self.action_edges[vertex][best_action]
        action_logloss = tensor_graph.contract(self.action_ids[vertex][best_action])

        trajectory_loss_pair = add_logedge_to_loss_pair(self._search(next_vertex, tensor_graph), action_logloss)
        trajectory_reward = -get_logloss(trajectory_loss_pair)

        reward_mu, reward_sigma = self.reward_distr_params[vertex]
        if reward_sigma > 1e-8:
            normalized_reward = (trajectory_reward - reward_mu) / reward_sigma
        else:
            normalized_reward = 0.0

        self.Qsa[vertex][best_action] = (self.Nsa[vertex][best_action] * self.Qsa[vertex][
            best_action] + normalized_reward) / (self.Nsa[vertex][best_action] + 1)
        self.Nsa[vertex][best_action] += 1
        self.Ns[vertex] += 1
        return trajectory_loss_pair
