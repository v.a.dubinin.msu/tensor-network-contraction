from typing import NamedTuple, Callable, Any, Tuple, List
import numpy as np
import itertools

import shutil
import pickle
from pathlib import Path

import haiku as hk
import jax
import jax.numpy as jnp
import optax
import jraph

import wandb

from tensor_networks.gnn import gnn_definition
from tensor_networks.testing.graph_generators import fully_connected_graph
from tensor_networks.utils import Graph, graph_to_tuple, my_batch, convert_tuple_to_jnp, my_segment_logsoftmax, get_segment_ids
from tensor_networks.alpha_zero.net.actor import actor_loss_fn
from tensor_networks.alpha_zero.net.critic import critic_loss_fn


class ModelState(NamedTuple):
    params: hk.Params
    opt_state: Any


class Batch(NamedTuple):
    observations: jraph.GraphsTuple
    mcts_policies: jnp.ndarray
    actions: jnp.ndarray
    rewards: jnp.ndarray


def init_net(net: Callable[..., Any], rng: jax.random.PRNGKey,
             optimizer_lr: float, dummy_input: jnp.array) \
             -> Tuple[ModelState, Callable[..., Any], optax.GradientTransformation]:
    init, forward = hk.without_apply_rng(hk.transform(net))
    params = init(rng, dummy_input)
    optimizer = optax.adamw(learning_rate=optimizer_lr)
    opt_state = optimizer.init(params)
    return ModelState(params=params, opt_state=opt_state), forward, optimizer


class NetArgs(NamedTuple):
    actor_lr: float = 1e-3
    critic_lr: float = 1e-3
    batch_size: int = 100
    actor_weights_decay_coeff: float = 5e-5
    critic_weights_decay_coeff: float = 5e-5
    max_nodes_count: int = 20
    max_edges_count: int = 40
    embedding_dimension: int = 16
    update_dimension: int = 10
    update_layers_count: int = 3
    num_message_passing_steps: int = 5


class AlphaZeroNet:
    def __init__(self,
                 rng: jax.random.PRNGKey,
                 args: NetArgs,
                 actor_state: ModelState = None,
                 critic_state: ModelState = None
                 ):
        self.rng = rng
        self.args = args
        self._init_params(args)

        self.rng, actor_key, critic_key = jax.random.split(self.rng, 3)
        dummy_graph_tuple = graph_to_tuple(fully_connected_graph(2, 1))

        def fixed_gnn(graph: jraph.GraphsTuple):
            return gnn_definition(graph, args.embedding_dimension, args.update_dimension, args.update_layers_count, args.num_message_passing_steps)

        self.actor_state, self.init_actor_forward, self.actor_optimizer = init_net(fixed_gnn, actor_key,
                                                                                   self.actor_lr, dummy_graph_tuple)
        self.critic_state, self.init_critic_forward, self.critic_optimizer = init_net(fixed_gnn, critic_key,
                                                                                      self.critic_lr, dummy_graph_tuple)
        if not (actor_state is None):
            self.actor_state = actor_state
        if not (critic_state is None):
            self.critic_state = critic_state

    def _init_params(self, args):
        self.max_nodes_count = args.max_nodes_count
        self.max_edges_count = args.max_edges_count
        self.batch_size = args.batch_size

        self.actor_lr = args.actor_lr
        self.critic_lr = args.critic_lr
        self.actor_weights_decay_coeff = args.actor_weights_decay_coeff
        self.critic_weights_decay_coeff = args.critic_weights_decay_coeff

        self.embedding_dimension = args.embedding_dimension
        self.update_dimension = args.update_dimension
        self.update_layers_count = args.update_layers_count
        self.num_message_passing_steps = args.num_message_passing_steps

    def init(self):
        trajectory_edges_total_count = self.batch_size * self.max_edges_count

        actor_loss = lambda actor_params, batch: actor_loss_fn(self.init_actor_forward, actor_params, batch,
                                                               trajectory_edges_total_count,
                                                               self.actor_weights_decay_coeff)
        critic_loss = lambda critic_params, batch: critic_loss_fn(self.init_critic_forward, critic_params, batch,
                                                                  trajectory_edges_total_count,
                                                                  self.critic_weights_decay_coeff)

        def sgd_step(loss_fn: Callable[..., Any], model_state: ModelState,
                     optimizer: optax.GradientTransformation, batch: Batch) -> ModelState:
            gradients = jax.grad(loss_fn, argnums=0)(model_state.params, batch)
            updates, new_opt_state = optimizer.update(gradients, model_state.opt_state)
            new_params = optax.apply_updates(model_state.params, updates)
            return ModelState(params=new_params, opt_state=new_opt_state)

        actor_sgd_step = jax.jit(
            lambda actor_state, batch: sgd_step(actor_loss, actor_state, self.actor_optimizer, batch))
        critic_sgd_step = jax.jit(
            lambda critic_state, batch: sgd_step(critic_loss, critic_state, self.critic_optimizer, batch))

        def _update(actor_state: ModelState, critic_state: ModelState, batch: Batch) -> Tuple[ModelState, ModelState]:
            new_critic_state = critic_sgd_step(critic_state, batch)
            new_actor_state = actor_sgd_step(actor_state, batch)
            return new_actor_state, new_critic_state

        self.actor_forward = jax.jit(self.init_actor_forward)
        self.critic_forward = jax.jit(self.init_critic_forward)
        self._update = _update

        def _process(graph_tuple, actor_forward, critic_forward, actor_params, critic_params):
            logits = actor_forward(actor_params, graph_tuple).squeeze()
            _, segment_ids = get_segment_ids(graph_tuple.n_edge, logits.shape[0])
            softmax = my_segment_logsoftmax(logits, segment_ids, len(graph_tuple.n_node),
                                            indices_are_sorted=True, unique_indices=False, return_logsoftmax=False)
            q_value = critic_forward(critic_params, graph_tuple).squeeze()
            return softmax, q_value

        self._process = jax.jit(lambda graph_tuple, actor_params, critic_params:
                                _process(graph_tuple, self.actor_forward, self.critic_forward, actor_params,
                                         critic_params))

    def deinit(self):
        self.actor_forward = None
        self.critic_forward = None
        self._update = None
        self._process = None

    def train(self, examples: List[Tuple]):
        batch = self._prepared_batch(examples)
        self.actor_state, self.critic_state = self._update(self.actor_state, self.critic_state, batch)

    def _prepared_batch(self, examples: List[Tuple]) -> Batch:
        observations, mcts_policies, actions, rewards = zip(*examples)
        mcts_policies = list(itertools.chain.from_iterable(mcts_policies))
        mcts_policies = mcts_policies + [0 for _ in range(self.max_edges_count * self.batch_size - len(mcts_policies))]
        tuple_observations = [graph_to_tuple(observation) for observation in observations]
        padded_observation_tuple = jraph.pad_with_graphs(my_batch(tuple_observations, np_=np),
                                                         n_node=self.max_nodes_count * self.batch_size + 1,
                                                         n_edge=self.max_edges_count * self.batch_size,
                                                         n_graph=self.batch_size + 1)
        return Batch(actions=jnp.array(actions, dtype=jnp.int32),
                     rewards=jnp.array(rewards, dtype=jnp.float32),
                     mcts_policies=jnp.array(mcts_policies, dtype=jnp.float32),
                     observations=convert_tuple_to_jnp(padded_observation_tuple))

    def predict(self, graph: Graph) -> Tuple[jnp.array, jnp.array]:
        graph_tuple = jraph.pad_with_graphs(graph_to_tuple(graph, np_=np),
                                            n_node=self.max_nodes_count + 1,
                                            n_edge=self.max_edges_count,
                                            n_graph=2)
        softmax, q_value = self._process(graph_tuple, self.actor_state.params, self.critic_state.params)
        softmax, q_value = softmax[:graph_tuple.n_edge[0]], q_value[:graph_tuple.n_edge[0]]
        softmax /= np.sum(softmax)
        return softmax, q_value


def save_net(net: AlphaZeroNet, path: str):
    path += '/net'
    Path(path).mkdir(parents=True, exist_ok=True)

    def save_object(object: Any, full_path: str):
        with open(full_path, 'wb') as fp:
            pickle.dump(object, fp)

    save_object(net.actor_state, path + '/actor_state.params')
    save_object(net.critic_state, path + '/critic_state.params')
    save_object(net.args, path + '/net_args.params')


def load_net(path: str) -> AlphaZeroNet:
    path += '/net'

    def load_object(full_path: str) -> Any:
        with open(full_path, 'rb') as fp:
            return pickle.load(fp)

    loaded_actor_state = load_object(path + '/actor_state.params')
    loaded_critic_state = load_object(path + '/critic_state.params')
    loaded_net_args = load_object(path + '/net_args.params')
    net = AlphaZeroNet(rng=jax.random.PRNGKey(42),
                       args=loaded_net_args,
                       actor_state=loaded_actor_state,
                       critic_state=loaded_critic_state)
    return net


def load_net_from_wandb(run_path: str) -> AlphaZeroNet:
    shutil.rmtree('./net', ignore_errors=True)
    wandb.restore('net/actor_state.params', run_path=run_path)
    wandb.restore('net/critic_state.params', run_path=run_path)
    wandb.restore('net/net_args.params', run_path=run_path)
    return load_net('.')
