import jax
import jax.numpy as jnp

from tensor_networks.utils import my_partition_softmax, get_segment_ids


def actor_loss_fn(actor_forward, actor_params, batch, trajectory_edges_total_count,
                  weights_decay_coeff) -> float:
    observations = batch.observations
    batch_size = len(observations.n_node) - 1  # Minus one due to jraph padding - last graph in batch is only needed
                                               # for making nodes and edges count the same for all batches.
    logits = actor_forward(actor_params, observations).squeeze()
    log_softmax = my_partition_softmax(logits, observations.n_edge,
                                      sum_partitions=trajectory_edges_total_count, return_logsoftmax=True)
    n_partitions, segment_ids = get_segment_ids(observations.n_edge, trajectory_edges_total_count)
    mean_cross_entropy = jnp.mean(jax.ops.segment_sum(-jnp.multiply(batch.mcts_policies.reshape(-1), log_softmax),
                                                      segment_ids, n_partitions, indices_are_sorted=True)[:-1])
    # weigths_square_norm = jnp.mean(jnp.array(jax.tree_leaves(jax.tree_map(lambda x: jnp.mean(x ** 2), actor_params))))
    actor_loss = mean_cross_entropy # + weights_decay_coeff * weigths_square_norm
    return actor_loss
