import jax
import jax.numpy as jnp


def critic_loss_fn(critic_forward, critic_params, batch, trajectory_edges_total_count, weights_decay_coeff) -> float:
    observations = batch.observations
    batch_size = len(observations.n_node) - 1  # Minus one due to jraph padding - last graph in batch is only needed
                                               # for making nodes and edges count the same for all batches.
    q_a = critic_forward(critic_params, observations).squeeze()
    action_indices = jnp.array(batch.actions) + jnp.cumsum(jnp.roll(observations.n_edge[:-1], 1).at[0].set(0))
    predicted_action_values = q_a[action_indices]
    mean_prediction_error = jnp.mean((predicted_action_values - batch.rewards) ** 2)
    # weights_square_norm = jnp.mean(jnp.array(jax.tree_leaves(jax.tree_map(lambda x: jnp.mean(x ** 2), critic_params))))
    critic_loss = mean_prediction_error # + weights_decay_coeff * weights_square_norm
    return critic_loss
