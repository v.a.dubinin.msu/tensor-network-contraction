import jax
import haiku as hk
import jraph


def double_gnn(graph: jraph.GraphsTuple,
               num_message_passing_steps: int = 5):
    return gnn_definition(graph, num_message_passing_steps), gnn_definition(graph, num_message_passing_steps)


def gnn_definition(
    graph: jraph.GraphsTuple,
    embedding_dimension: int = 16,
    update_dimension: int = 10,
    update_layers_count: int = 3,
    num_message_passing_steps: int = 5) -> jraph.ArrayTree:
    """Defines a graph neural network.
    Args:
    graph: Graphstuple the network processes.
    num_message_passing_steps: number of message passing steps.
    Returns:
    Decoded nodes.
    """
    embedding = jraph.GraphMapFeatures(
        embed_edge_fn=jax.vmap(hk.Linear(output_size=embedding_dimension)),
        embed_node_fn=jax.vmap(hk.Linear(output_size=embedding_dimension))
    )
    graph = embedding(graph)

    @jax.vmap
    @jraph.concatenated_args
    def update_fn(features):
        layers_list = []
        for _ in range(update_layers_count):
            layers_list.append(hk.Linear(update_dimension))
            layers_list.append(jax.nn.relu)
        net = hk.Sequential(layers_list)
        return net(features)

    for _ in range(num_message_passing_steps):
        gn = jraph.InteractionNetwork(
            update_edge_fn=update_fn,
            update_node_fn=update_fn,
            include_sent_messages_in_node_update=True
        )
        graph = gn(graph)

    return hk.Linear(1)(graph.edges)


def gnn_with_global_definition(
    graph: jraph.GraphsTuple,
    num_message_passing_steps: int = 5) -> jraph.ArrayTree:
    """Defines a graph neural network.
    Args:
    graph: Graphstuple the network processes.
    num_message_passing_steps: number of message passing steps.
    Returns:
    Decoded nodes.
    """
    embedding = jraph.GraphMapFeatures(
        embed_edge_fn=jax.vmap(hk.Linear(output_size=16)),
        embed_node_fn=jax.vmap(hk.Linear(output_size=16)),
        embed_global_fn=jax.vmap(hk.Linear(output_size=16))
    )
    graph = embedding(graph)

    @jax.vmap
    @jraph.concatenated_args
    def update_fn(features):
        net = hk.Sequential([
            hk.Linear(10), jax.nn.relu,
            hk.Linear(10), jax.nn.relu,
            hk.Linear(10), jax.nn.relu])
        return net(features)

    for _ in range(num_message_passing_steps):
        gn = jraph.GraphNetwork(
            update_edge_fn=update_fn,
            update_node_fn=update_fn,
            update_global_fn=update_fn)
        graph = gn(graph)

    return hk.Linear(1)(graph.edges), hk.Linear(1)(graph.globals)
