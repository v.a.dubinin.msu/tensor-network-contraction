import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import pylab

from tensor_networks.train import AgentTrajectory
from tensor_networks.utils import Graph


def draw_graph_distribution(my_graph: Graph, distr: np.ndarray, action_edge_id: int):
    plt.figure(figsize=(10, 6))
    nx_graph = nx.Graph()
    for edge_idx, (edge, prob) in enumerate(zip(my_graph.edges, distr)):
        nx_graph.add_edges_from([(edge.u, edge.v)], probability=round(prob, 2), dim=edge.dim,
                                id=edge.id, style='-' * (1 + (edge_idx == action_edge_id)))
    probabilities = nx.get_edge_attributes(nx_graph, 'probability')
    prob_values = probabilities.values()
    dims = nx.get_edge_attributes(nx_graph, 'dim')
    dims_values = dims.values()
    styles = nx.get_edge_attributes(nx_graph, 'style').values()

    pos = nx.spring_layout(nx_graph)

    nx.draw_networkx_edge_labels(nx_graph, pos, edge_labels=probabilities, label_pos=0.67, font_size=13)
    nx.draw_networkx_edge_labels(nx_graph, pos, edge_labels=dims, label_pos=0.33, font_size=9)

    nx.draw(nx_graph, pos, node_color='y', width=list(dims_values),
            with_labels=True, style=styles, edge_color=prob_values, edge_cmap=plt.cm.get_cmap('Oranges'))
    pylab.show()


def draw_agent_trajectory(trajectory: AgentTrajectory):
    for graph, distr, action in zip(trajectory.observations[:-1], trajectory.distributions, trajectory.chosen_actions):
        draw_graph_distribution(graph, distr, action)
