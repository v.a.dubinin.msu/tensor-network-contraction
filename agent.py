from typing import Any, Callable, NamedTuple, Tuple, Union
import numpy as np
import enum

from bsuite.baselines import base
from bsuite.baselines.utils import sequence

import dm_env
import haiku as hk
import jax
import jax.numpy as jnp
import optax
import jraph
import rlax

from tensor_networks.gnn import gnn_definition, gnn_with_global_definition
from tensor_networks.testing.graph_generators import fully_connected_graph
from tensor_networks.utils import graph_to_tuple, DummyBuffer, my_partition_softmax, GraphType, my_batch


Logits = jnp.ndarray
PolicyValueNet = Callable[[jnp.ndarray], Logits]


class TrainingState(NamedTuple):
    params: hk.Params
    opt_state: Any


class AgentType(enum.Enum):
    reinforce = 0
    actor_critic = 1
    unknown = 2


class ReinforceAgent:
    """Feed-forward actor-critic agent.py."""

    def __init__(
            self,
            network: PolicyValueNet,
            optimizer: optax.GradientTransformation,
            rng: hk.PRNGSequence,
            discount: float,
            max_nodes_count: int,
            max_edges_count: int,
            agent_type: AgentType = AgentType.reinforce
    ):
        trajectory_edges_total_count = max_nodes_count * max_edges_count

        # Transform the loss into a pure function.
        if agent_type == AgentType.reinforce:
            loss_fn = lambda trajectory: loss_reinforce(trajectory, network, discount, trajectory_edges_total_count)
        elif agent_type == AgentType.actor_critic:
            loss_fn = lambda trajectory: loss_actor_critic(trajectory, network, discount, trajectory_edges_total_count)
        else:
            raise Exception("Please, set known agent type")

        loss_fn = hk.without_apply_rng(hk.transform(loss_fn, apply_rng=True)).apply
        # Define update function.
        @jax.jit
        def sgd_step(state: TrainingState,
                     trajectory: sequence.Trajectory) -> TrainingState:
            """Does a step of SGD over a trajectory."""
            gradients = jax.grad(loss_fn)(state.params, trajectory)
            updates, new_opt_state = optimizer.update(gradients, state.opt_state)
            new_params = optax.apply_updates(state.params, updates)
            return TrainingState(params=new_params, opt_state=new_opt_state)

        # Initialize network parameters and optimiser state.
        init, forward = hk.without_apply_rng(hk.transform(network, apply_rng=True))
        dummy_graph = fully_connected_graph(2, 1)
        dummy_graph_tuple = graph_to_tuple(dummy_graph)
        initial_params = init(next(rng), dummy_graph_tuple)
        initial_opt_state = optimizer.init(initial_params)

        # Internalize state.
        self._state = TrainingState(initial_params, initial_opt_state)
        self._forward = jax.jit(forward)
        self._buffer = DummyBuffer()
        self._sgd_step = sgd_step
        self._rng = rng
        self.agent_type = agent_type
        self.max_nodes_count = max_nodes_count
        self.max_edges_count = max_edges_count

    def select_action(self, timestep: dm_env.TimeStep, select_best: bool = False,
                      return_logits: bool = False) -> Union[int, Tuple[int, jnp.ndarray]]:
        """Selects actions according to a softmax policy."""
        key = next(self._rng)
        observation = jraph.pad_with_graphs(timestep.observation,
                                            n_node=self.max_nodes_count + 1,
                                            n_edge=self.max_edges_count,
                                            n_graph=2)
        if self.agent_type == AgentType.reinforce:
            logits = self._forward(self._state.params, observation).squeeze()[:timestep.observation.n_edge[0]]
        elif self.agent_type == AgentType.actor_critic:
            logits, _ = self._forward(self._state.params, observation)
            logits = logits.squeeze()[:timestep.observation.n_edge[0]]
        logits = logits.reshape(1, -1)
        if select_best:
            action = jax.numpy.argmax(logits).squeeze()
        else:
            action = jax.random.categorical(key, logits).squeeze()
        if return_logits:
            return int(action), logits.reshape(-1)
        return int(action)

    def update(
            self,
            timestep: dm_env.TimeStep,
            action: int,
            new_timestep: dm_env.TimeStep,
    ):
        """Adds a transition to the trajectory buffer and periodically does SGD."""
        self._buffer.append(timestep, action, new_timestep)
        if new_timestep.last():
            trajectory = self._buffer.drain()
            prepared_trajectory = self._prepared_trajectory(trajectory)
            self._state = self._sgd_step(self._state, prepared_trajectory)

    def _prepared_trajectory(self, trajectory: sequence.Trajectory):
        return sequence.Trajectory(actions=trajectory.actions,
                                   rewards=trajectory.rewards,
                                   discounts=trajectory.discounts,
                                   observations=jraph.pad_with_graphs(my_batch(trajectory.observations, np_=np),
                                                                      n_node=self.max_nodes_count * self.max_nodes_count + 1,
                                                                      n_edge=self.max_edges_count * self.max_nodes_count,
                                                                      n_graph=len(trajectory.observations) + 1))


def default_reinforce_agent(max_nodes_count: int, max_edges_count: int, seed: int = 0) -> base.Agent:
    return ReinforceAgent(
        network=gnn_definition,
        optimizer=optax.adam(3e-3),
        rng=hk.PRNGSequence(seed),
        discount=0.99,
        max_nodes_count=max_nodes_count,
        max_edges_count=max_edges_count,
        agent_type=AgentType.reinforce
    )


def default_actor_critic_agent(max_nodes_count: int, max_edges_count: int, seed: int = 0) -> base.Agent:
    return ReinforceAgent(
        network=gnn_with_global_definition,
        optimizer=optax.adam(3e-3),
        rng=hk.PRNGSequence(seed),
        discount=0.99,
        max_nodes_count=max_nodes_count,
        max_edges_count=max_edges_count,
        agent_type=AgentType.actor_critic
    )


def loss_reinforce(trajectory: sequence.Trajectory, network: PolicyValueNet, discount: float,
                   trajectory_edges_total_count: int) -> jnp.ndarray:
    """"Reinforce loss."""

    concatenated_tuple = trajectory.observations
    logits = network(concatenated_tuple).squeeze()
    log_softmax = my_partition_softmax(logits, concatenated_tuple.n_edge,
                                       sum_partitions=trajectory_edges_total_count)
    rewards = jnp.array(trajectory.rewards)
    discount_powers = jnp.array([discount ** i for i in range(len(trajectory.rewards))])
    reverse_rewards = jnp.flip(rewards)
    adv_t = (jnp.convolve(reverse_rewards, discount_powers)[:len(rewards)])[::-1]
    adv_t = jax.lax.stop_gradient(adv_t)

    action_indices = jnp.array(trajectory.actions) + jnp.cumsum(jnp.roll(concatenated_tuple.n_edge[:-2], 1).at[0].set(0))
    log_policy_actions = log_softmax[action_indices]
    return jnp.mean(-log_policy_actions * adv_t)


def loss_actor_critic(trajectory: sequence.Trajectory, network: PolicyValueNet, discount: float,
                      trajectory_edges_total_count: int) -> jnp.ndarray:
    """"Actor-critic loss."""
    concatenated_tuple = trajectory.observations
    logits, value_function = network(concatenated_tuple)
    value_function = jnp.concatenate([value_function.squeeze()[:-2], jnp.zeros(1)])
    td_errors = rlax.td_lambda(
        v_tm1=jnp.array(value_function[:-1]),
        r_t=jnp.array(trajectory.rewards),
        discount_t=jnp.array(trajectory.discounts) * jnp.float32(discount),
        v_t=jnp.array(value_function[1:]),
        lambda_=jnp.float32(0.9),
    )
    critic_loss = jnp.mean(td_errors ** 2)
    logits = logits.squeeze()
    log_softmax = my_partition_softmax(logits, concatenated_tuple.n_edge,
                                       sum_partitions=trajectory_edges_total_count)
    adv_t = jax.lax.stop_gradient(td_errors)
    action_indices = jnp.array(trajectory.actions) + jnp.cumsum(jnp.roll(concatenated_tuple.n_edge[:-2], 1).at[0].set(0))
    log_policy_actions = log_softmax[action_indices]
    actor_loss = jnp.mean(-log_policy_actions * adv_t)
    return actor_loss + critic_loss
